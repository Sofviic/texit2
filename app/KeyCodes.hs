{-# LANGUAGE OverloadedStrings #-}
module KeyCodes where

import Monomer
import Prelude as P
import Data.Char
import Data.List as L
import Data.Text as T

allt :: Foldable t => a -> t (a -> Bool) -> Bool
allt x = L.foldr ((&&) . ($ x)) True

anyt :: Foldable t => a -> t (a -> Bool) -> Bool
anyt x = L.foldr ((||) . ($ x)) False

firstKey' :: (KeyCode -> Bool) -> [KeyCode] -> Char
firstKey' p = key2char . P.head . P.filter p

(+++) :: Text -> Text -> Text
(+++) = append

key2char :: KeyCode -> Char
key2char = chr . unKeyCode

keyname :: KeyCode -> Text
keyname k | isKeyAlphanum k     = T.singleton . key2char $ k
keyname k | isKeyEscape k       = "Esc"
keyname k | isKeyBackspace k    = "Backspace"
keyname k | isKeySpace k        = "Space"
keyname k = T.pack . show $ k

showKeySeq :: [KeyCode] -> Text
showKeySeq ks = id
                . (prefixIf isKeyShift "S" ks +++)
                . (prefixIf isKeyCtrl "C" ks +++)
                . (prefixIf isKeyMeta "M" ks +++)
                . (prefixIf isKeyHyper "H" ks +++)
                . T.intercalate " " 
                . fmap keyname
                . P.filter isKeyAlphanum 
                $ ks
            where
                prefixIf :: (a -> Bool) -> Text -> [a] -> Text
                prefixIf p t a = if L.any p a then t +++ "-" else ""

keyShift,keyCtrl,keyMeta,keyHyper :: KeyCode
keyShift    = keyLShift
keyCtrl     = keyLCtrl
keyMeta     = keyLAlt
keyHyper    = keyLGUI

chordHas :: [KeyCode] -> [KeyCode] -> Bool
chordHas = L.all . flip L.elem

keyChord :: [KeyCode] -> [KeyCode]
keyChord ks = id
            . (prefixIf isKeyShift [keyShift] ks ++)
            . (prefixIf isKeyCtrl [keyCtrl] ks ++)
            . (prefixIf isKeyMeta [keyMeta] ks ++)
            . (prefixIf isKeyHyper [keyHyper] ks ++)
            . L.take 1
            . P.filter (not . isKeyMod)
            $ ks
        where
            prefixIf :: (a -> Bool) -> [b] -> [a] -> [b]
            prefixIf p t a = if L.any p a then t else []

keySort :: [KeyCode] -> [KeyCode]
keySort = sortOn isKeyMod

isKeyMod :: KeyCode -> Bool
isKeyMod = flip anyt [isKeyShift, isKeyCtrl, isKeyMeta, isKeyHyper]

isKeyShift :: KeyCode -> Bool
isKeyShift = flip anyt [isKeyLShift, isKeyRShift]
isKeyCtrl :: KeyCode -> Bool
isKeyCtrl = flip anyt [isKeyLCtrl, isKeyRCtrl]
isKeyMeta :: KeyCode -> Bool
isKeyMeta = flip anyt [isKeyLAlt, isKeyRAlt]
isKeyHyper :: KeyCode -> Bool
isKeyHyper = flip anyt [isKeyLGUI, isKeyRGUI]

isKeyAlphanum :: KeyCode -> Bool
isKeyAlphanum = flip anyt 
                    [isKeyA,
                    isKeyB,
                    isKeyC,
                    isKeyD,
                    isKeyE,
                    isKeyF,
                    isKeyG,
                    isKeyH,
                    isKeyI,
                    isKeyJ,
                    isKeyK,
                    isKeyL,
                    isKeyM,
                    isKeyN,
                    isKeyO,
                    isKeyP,
                    isKeyQ,
                    isKeyR,
                    isKeyS,
                    isKeyT,
                    isKeyU,
                    isKeyV,
                    isKeyW,
                    isKeyX,
                    isKeyY,
                    isKeyZ,
                    isKey0,
                    isKey1,
                    isKey2,
                    isKey3,
                    isKey4,
                    isKey5,
                    isKey6,
                    isKey7,
                    isKey8,
                    isKey9]

