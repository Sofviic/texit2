module Diff where

data Diff a = Insert a Integer | Remove Integer deriving (Eq,Show,Read)
type Diffs a = [Diff a]

type State = (Diffs Char, Integer)

collapseDiffs :: Diffs a -> [a]
collapseDiffs = foldr collapseDiff []

collapseDiff :: Diff a -> [a] -> [a]
collapseDiff (Insert a i) as = take (fromInteger i  ) as ++ [a] ++ drop (fromInteger i) as
collapseDiff (Remove   i) as = take (fromInteger i-1) as        ++ drop (fromInteger i) as

insertc :: Char -> State -> State
insertc c (d,i) = mover (insert d c i,i)

removec :: State -> State
removec (d,i) = movel (remove d i,i)

mover,movel :: State -> State
mover = move 1
movel = move (-1)

move :: Integer -> State -> State
move n (d,i) = (d,clamp 0 (len d) (i+n))

newtext :: State
newtext = (newdiff,0)

len :: Diffs a -> Integer
len = foldr (+) 0 . fmap inserts
        where inserts (Insert _ _) = 1
              inserts (Remove _  ) = 0

newdiff :: Diffs a
newdiff = []

insert :: Diffs a -> a -> Integer -> Diffs a
insert = (.#. Insert) . flip (:)

remove :: Diffs a -> Integer -> Diffs a
remove = (. Remove) . flip (:)

clamp :: Ord a => a -> a -> a -> a
clamp a b x | x < a     = a
            | b < x     = b
            | otherwise = x


infixr 9 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g


