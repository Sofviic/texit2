{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import Diff
import KeyCodes
import Monomer
import Prelude as P
import Data.Char as C
import Data.Text as T
import Data.Map.Strict (foldrWithKey)

data Event = AppInit | AppQuit 
            | AppSave | AppLoad
            | AppState State
            | AppType Char 
            | AppBack 
            | AppL | AppR 
            | AppIO 
            | Null deriving (Eq,Show)

filepath :: FilePath
filepath = "file.texit2"

saveTexit :: State -> IO Event
saveTexit s = do writeFile filepath $ show s
                 return Null

loadTexit :: IO Event
loadTexit = do s <- readFile filepath
               return $ AppState $ read s

evth :: AppEventHandler State Event
evth _env _rwidget _state AppInit     = [Model newtext, Event AppIO]
evth _env _rwidget _state AppQuit     = [Request $ ExitApplication True]
evth _env _rwidget  state AppSave     = [Task $ saveTexit state, Event AppIO]
evth _env _rwidget _state AppLoad     = [Task $ loadTexit, Event AppIO]
evth _env _rwidget _state (AppState s)= [Model s, Event AppIO]
evth _env _rwidget  state (AppType c) = [Model $ insertc c state, Event AppIO]
evth _env _rwidget  state AppBack     = [Model $ removec state, Event AppIO]
evth _env _rwidget  state AppL        = [Model $ movel state, Event AppIO]
evth _env _rwidget  state AppR        = [Model $ mover state, Event AppIO]
evth  env _rwidget _state AppIO       = [Task $ keypress env]
evth _env _rwidget _state Null        = []

if' :: Bool -> a -> a -> a
if' True  a _ = a
if' False _ a = a

passboth :: a -> (a -> b, a -> c) -> (b,c)
passboth x (f,g) = (f x, g x)

keyevents :: [([KeyCode] -> Bool, [KeyCode] -> Event)]
keyevents = [
            (flip allt [P.any isKeyCtrl, P.any isKeyS]  , const AppSave),
            (flip allt [P.any isKeyCtrl, P.any isKeyL]  , const AppLoad),
            (flip allt [P.any isKeyCtrl, P.any isKeyN]  , const AppInit),
            (P.any isKeyEscape                          , const AppQuit),
            (P.any isKeyBackspace                       , const AppBack),
            (P.any isKeyLeft                            , const AppL),
            (P.any isKeyRight                           , const AppR),
            (flip allt [not . P.null . P.filter isKeyAlphanum
                , not . P.null . P.filter isKeyShift]   , AppType . C.toUpper . firstKey' isKeyAlphanum),
            (not . P.null . P.filter isKeyAlphanum      , AppType . firstKey' isKeyAlphanum)
            ]

handleKeys :: [KeyCode] -> Event
handleKeys ks = P.foldr (uncurry if' . passboth ks) AppIO keyevents

keypress :: WidgetEnv State Event -> IO Event
keypress = return
            . handleKeys
            . foldrWithKey (\k a -> if a == KeyPressed then (k:) else id) [] 
            . _ipsKeys 
            . _weInputStatus

map' :: (a -> a) -> [a] -> [a]
map' _ []       = []
map' _ [x]      = [x]
map' f (x:xs)   = f x : map' f xs

caret :: Int -> Text -> Text
caret i t = T.take i t +++ "\x2588" +++ T.drop i t

ui :: UIBuilder State Event
ui _env (diffs, i) = vstack $ [
                        label "Esc = Quit, ",
                        label "C-s = Save, C-l = Load, C-n = New, ",
                        label "Left/Right = Cursor Move, ",
                        label "Del = Remove Char, [a-zA-Z0-9] = Type",
                        label linebreak,
                        label "Text Area:",
                        label linebreak
                        ] ++ fmap label texts
        where 
            texts = map' (+++ "\x2193") . chunksOf 79 . caret (fromInteger i) . T.pack . collapseDiffs $ diffs
            linebreak = T.replicate 80 "="

main :: IO ()
main = startApp newtext evth ui config

config :: [AppConfig s Event]
config = [
            appWindowTitle "Texit 2",
            appTheme darkTheme,
            appFontDef "Regular" "./src/fonts/DejaVuSansMono.ttf",
            appInitEvent AppInit
            ]